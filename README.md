# Carter Perkins carterp@uoregon.edu
## Description
This is a Python flask webserver implementing JQuery, Javascript, and AJAX to create an ACP Brevet Control Times Calculator like the one described on www.rusa.org. This server checks implements AJAX in the calculator to get the opening and closing time for the brevets in real time. JQuery and JSON are used to transfer data between the server and the client's webpage, while Python is used for the server itself as well as the underlying logic for the time calculation.
## Running the Server
`sudo docker build <name> .`
`sudo docker run -d -p 5000:5000 <name>`
`localhost:5000`
## Testing
`cd brevets`
`nosetests`

