import nose
import acp_times
import arrow

START_TIME = arrow.get('2017-01-01 00:00').isoformat()
BREVETS = [(0, 200, 15, 34), (200, 400, 15, 32), (400, 600, 15, 30), (600, 1000, 11.428, 28), (1000, 1300, 13.333, 26)]
FINISH_BREVETS = [(200, 13, 30), (300, 20, 0), (400, 27, 0), (600, 40, 0), (1000, 75, 0)]

def test_finish_closing_times():
    '''Test the finish line closing times.
    '''
    for B in FINISH_BREVETS:
        assert acp_times.close_time(B[0], B[0], START_TIME) == arrow.get(START_TIME).shift(hours=+B[1], minutes=+B[2]).isoformat()
    
def test_start_closing_time():
    '''Test the start line closing times.
    '''
    for B in FINISH_BREVETS:
        assert acp_times.close_time(0, B[0], START_TIME) == arrow.get(START_TIME).shift(hours=+1).isoformat()

def test_start_opening_time():
    '''Test the start line opening times.
    '''
    for B in FINISH_BREVETS:
        assert acp_times.open_time(0, B[0], START_TIME) == arrow.get(START_TIME).isoformat()
        
def test_400_250():
    '''Test opening and closing times for a 400km distance with a brevet at 250km.
    '''
    assert acp_times.open_time(250, 400, START_TIME) == arrow.get('2017-01-01 07:27').isoformat()
    assert acp_times.close_time(250, 400, START_TIME) == arrow.get('2017-01-01 16:40').isoformat()
    
def test_1000_883():
    '''Test opening and closing times for a 1000km distance with a brevet at 883km. 
    '''
    assert acp_times.open_time(883, 1000, START_TIME) == arrow.get('2017-01-02 04:54').isoformat()
    assert acp_times.close_time(883, 1000, START_TIME) == arrow.get('2017-01-03 16:46').isoformat()
    
