"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


#          (LOW_LOCATION, HIGH_LOCATION, LOW_SPEED, MAX_SPEED)
BREVETS = [(0, 200, 15, 34), (200, 400, 15, 32), (400, 600, 15, 30), (600, 1000, 11.428, 28), (1000, 1300, 13.333, 26)]
#               (dist, hour, minutes)
FINISH_BREVETS = [(200, 13, 30), (300, 20, 0), (400, 27, 0), (600, 40, 0), (1000, 75, 0)]


 


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)
    if control_dist_km == 0:    # if starting brevet follow special case (starting time = open time)
        return brevet_start_time.isoformat()
    
    time_change = [0, 0, 0]
    change_time(control_dist_km, time_change, True)
     
    return brevet_start_time.shift(minutes=+time_change[0], hours=+time_change[1], days=+time_change[2]).isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    brevet_start_time = arrow.get(brevet_start_time)
    if control_dist_km == 0:    # if starting brevet follow sepcial case (closing time = (start + 1 hour)
        return brevet_start_time.shift(hours=+1).isoformat()
    if control_dist_km >= brevet_dist_km:   # if brevet is exactly the finishing or greater then it is the last brevet
        for B in FINISH_BREVETS:
            if B[0] == brevet_dist_km:
                print(B)
                return brevet_start_time.shift(hours=+B[1], minutes=+B[2]).isoformat()
    time_change = [0, 0, 0]
    change_time(control_dist_km, time_change, False)

    return brevet_start_time.shift(minutes=+time_change[0], hours=+time_change[1], days=+time_change[2]).isoformat()


def change_time(control_dist_km, time_change, open_time):
    '''
    Calculates the hours and minutes added to the start time for 
    brevet open and close time calculation.
    
    Args:
        control_dist_km: number, the control distance in kilometers
        time_change: list, hours and mintues
        open_time: bool, whether to calculate the open_time or close_time
    Returns:
        None
    Effects:
        changes the hours and minutes in time_change
    '''
    # (HOURS, MINUTES) keep track of time in order to calculate successive calls to BREVETS 
    # (i.e. 250 km = 200/34 + 50/32 ->  5H53 + 1H34 -> time_change = (6, 87) which is then
    # added to the start time (i.e. start time = 12H00 -> as arrow object -> start_time.replace(hours=+6, minutes=+87) 
    i = 0
    
    if open_time:
        speed = 3
    else:
        speed = 2
    
    while not control_dist_km <= 0: # i.e.  750 -> 200/34 + 200/32 + 200/30 + 150/28, i: 0, 1, 2, 3 loops: 4
        if control_dist_km > (BREVETS[i][1]-BREVETS[i][0]): # i.e. if control_dist_km is > 200 use 200 since that would be max value of current brevet bracket
            minutes, hours = math.modf(200 / BREVETS[i][speed]) # i.e. 200/34 set whole numbers as hours, decimal as fraction 
        else:
            minutes, hours = math.modf(control_dist_km / BREVETS[i][speed]) # i.e. 200/34 set whole numbers as hours, decimal as fraction 
        time_change[0] += round(minutes * 60)
        time_change[1] += round(hours)
        control_dist_km -= (BREVETS[i][1]-BREVETS[i][0])
        i += 1

    return time_change


#N = 600
#a = arrow.get('2017-01-01 00:00', 'YYYY-MM-DD HH:mm')   
#for i in range(0, N + 1, 100):
#    print(arrow.get(open_time(i, N, a)).format('YYYY-MM-DD HH:mm'), arrow.get(close_time(i, N, a)).format('YYYY-MM-DD HH:mm'))
